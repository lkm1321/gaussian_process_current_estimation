classdef OceanGPMM < handle
    properties (Constant)
        DEBUG = false;
    end

    properties
        gp
        gamma_gps
        num_iter
        dt
        zone        
    end    
    
    methods
        %% ctor
        % gamma_gps: the noise in GPS measurement
        % num_iter: number of iterations
        % dt: the time-step to use in EM
        % zone: the UTM zone for conversion
        function obj = OceanGPMM(gp, gamma_gps, num_iter, dt, zone)
            obj.gp = gp; 
            obj.gamma_gps = gamma_gps; 
            obj.num_iter = num_iter; 
            obj.dt = dt; 
            if (exist('zone', 'var'))
                obj.zone = zone;
            else
                obj.zone = '56H';
            end
        end
        
        %% compute corrected drifter trajectory and update ocean current estimate given drifter GPS data
        % this routine works in lat lon coordiantes
        % drifterTraj_ll: cell array of N x 3 where first two elements are en and the last time
        % drifterTraj_ll_corrected: same format, but corrected        
        function [drifterTraj_ll_corrected] = update_ll_continuous_multiple(obj, drifterTraj_ll)
            % copy for correct size
            drifterTraj_en = drifterTraj_ll;
            % convert ll to en
            for i = 1:numel(drifterTraj_ll)
                drifterTraj_en{i}(:, 1:2) = ll2utm(drifterTraj_ll{i}(:, 1:2));
            end
            
            drifterTraj_en_corrected = obj.update_en_continuous_multiple(drifterTraj_en);
            
            drifterTraj_ll_corrected = drifterTraj_en_corrected;
            for i = 1:numel(drifterTraj_en)
                drifterTraj_ll_corrected{i}(:, 1:2) = utm2ll(drifterTraj_en_corrected{i}(:, 1:2), obj.zone); 
            end
            
        end
        
        %% compute corrected drifter trajectory and update ocean current estimate given drifter GPS data
        % this routine works in utm coordiantes
        % drifterTraj: cell array of N x 3 where first two elements are en and the last time
        % drifterTraj_corrected: same format, but corrected
        function [drifterTraj_corrected] = update_en_continuous_multiple(obj, drifterTraj)
            x_minor_array = cell(numel(drifterTraj), 1);
            B_array = cell(numel(drifterTraj), 1); 
            C_array = cell(numel(drifterTraj), 1);
            x_dr_array = cell(numel(drifterTraj), 1);
            dx_dr_array = cell(numel(drifterTraj), 1);
            time_vector_inflated_array = cell(numel(drifterTraj), 1);
            
            for i = 1:numel(drifterTraj)
                [time_vector_inflated_array{i}, B_array{i}, C_array{i}] = obj.get_matrices_continuous(drifterTraj{i}(:, 3)); 
                [x_minor_array{i}, x_dr_array{i}, dx_dr_array{i}] = obj.get_measurements(drifterTraj{i}(:, 1:2), drifterTraj{i}(:, 3), time_vector_inflated_array{i}); 
            end
            
            x_minor_combined = vertcat(x_minor_array{:}); 
            B_combined = blkdiag(B_array{:});
            C_combined = blkdiag(C_array{:}); 
            x_dr_combined = vertcat(x_dr_array{:});
            dx_dr_combined = horzcat(dx_dr_array{:});
            
            for i = 1:obj.num_iter
                [u_pred, v_pred, x_minor_combined] = obj.do_em(x_minor_combined, ...
                                                               x_dr_combined, ...
                                                               dx_dr_combined, ...
                                                               B_combined, ...
                                                               C_combined);
                %% plots for debugging during EM iterations
                % figure(1); hold on;
                % plot(x_minor_combined(:, 1), x_minor_combined(:, 2));
                % hold off;
                % drawnow;
            end
            
            % add to GP for prediction
            obj.gp.addSample(x_minor_combined(:, 1), x_minor_combined(:, 2), u_pred(:), v_pred(:));

            % assemble the output
            drifterTraj_corrected = cell(size(drifterTraj));
            head = 0; 
            
            for i = 1:numel(drifterTraj_corrected)
                current_length = size(x_minor_array{i}, 1);
                drifterTraj_corrected{i} = [x_minor_combined(head + (1:current_length), :), time_vector_inflated_array{i}(1:(end-1))' ];
                head = head + current_length; 
            end
            
            
        end
        
        %% the main EM routine. Given the B and C matrices s.t.
        % (trajectory) = (dead reckoning) + B * (current)
        % (GPS measurements) = C * (trajectory)
        % solve for the corrected trajectory and current along correct trajectory
        function [u_pred, v_pred, x_corrected] = do_em(obj, x_minor, x_dr, dx_dr, B, C)
               [u, v, K] = obj.gp.predict(x_minor(:, 1), x_minor(:, 2));
               % Reformat to  y = [u1; v1; u2; v2 ...]               
               yp = [u(:), v(:)]';        
               yp = yp(:); 
               KC = K*C';

               % solve the linear system for identifying w_pred
               regressor = C*KC + 1/obj.gamma_gps * eye(size(C, 1)); 
               opts.SYM = true;
               % the regressor matrix should be sym posdef, but numerical
               % issues..
               % opts.POSDEF = true; 
               w_pred = yp + KC * linsolve(regressor, dx_dr(:) - C * yp, opts); 

               % The below is before applying Woodbury identity, the
               % conditioning is worse
               % w_pred = (obj.gamma_gps * (C'*C) + K^-1) \ (obj.gamma_gps * C' * dx_dr(:) + K \ yp);

               % Reformat y to u and v
               u_pred = w_pred(1:2:(end-1));
               v_pred = w_pred(2:2:end);
               
               % Compute the corrected trajectory
               offset = B * w_pred; 
               offset_x = offset(1:2:(end-1)); 
               offset_y = offset(2:2:end); 
               x_corrected = x_dr + [offset_x(:), offset_y(:)]; 
        end
        
        %% GP prediction wrappers
        
        % predict current vectors at grid points mLat mLon
        % outputs mU, mV: x-, y- coordinates of current vector
        % K_query: covariance in prediction, 
        % K_query computation can be skipped if nargout == 2
        function [mU, mV, K_query] = predict_ll(obj, mLat, mLon)
            mEN_global = ll2utm([mLat(:), mLon(:)]); 
            mE_global = mEN_global(:, 1); 
            mN_global = mEN_global(:, 2); 
            
            if (nargout > 2)
                [mU, mV, K_query] = obj.predict_en(mE_global, mN_global);
            else
                [mU, mV] = obj.predict_en(mE_global, mN_global);
            end
            
            mU = reshape(mU, size(mLat)); 
            mV = reshape(mV, size(mLon));
        end

        % predict current vectors at grid points mE, mN
        % outputs mU, mV: x-, y- coordinates of current vector
        % K_query: covariance in prediction, 
        % K_query computation can be skipped if nargout == 2
        function [mU, mV, K_query] = predict_en(obj, mE_global, mN_global)                        
            if (nargout > 2)
                [mU, mV, K_query] = obj.gp.predict(mE_global, mN_global); 
            else
                [mU, mV] = obj.gp.predict(mE_global, mN_global);                 
            end
            mU = reshape(mU, size(mE_global)); 
            mV = reshape(mV, size(mN_global)); 
        end

        % predict streamfunction value at grid points mLat mLon
        % outputs mPhi: streamfunction value at mLat mLon
        % mE, mN: (obsolete) the UTM coordinates
        function [mPhi, mE, mN] = predictPhi_ll(obj, mLat, mLon)
           mEN_global = ll2utm([mLat(:), mLon(:)]); 
           mE_global = mEN_global(:, 1);
           mN_global = mEN_global(:, 2); 
           [mPhi, mE, mN] = obj.predictPhi_en(mE_global, mN_global);
            mPhi = reshape(mPhi, size(mLat)); 
        end

        % predict streamfunction value at grid points mE mN
        % outputs mPhi: streamfunction value at mLat mLon
        % mE, mN: (obsolete) the UTM coordinates        
        function [mPhi, mE_global, mN_global] = predictPhi_en(obj, mE_global, mN_global)
           mPhi = obj.gp.predictPhi(mE_global, mN_global); 
           mPhi = reshape(mPhi, size(mE_global)); 
        end
        
        % generate prediction in ocean current struct format
        function [out_struct, K] = get_struct(obj, in_struct)
            
            mLat = in_struct.mLat(:, :, 1); 
            mLon = in_struct.mLon(:, :, 1); 
            
            [mU, mV, K] = obj.predict_ll(mLat, mLon); 
            
            out_struct = in_struct; 
            
            for i = 1:size(out_struct.mU, 3)
               out_struct.mU(:, :, i) = mU; 
               out_struct.mV(:, :, i) = mV; 
            end
        end
        
    end
    
    %% private helper methods
    methods (Access = private)

        %% Construct matrices B and C s.t.
        % (trajectory) = (dead reckoning) + B * (current)
        % (GPS measurements) = C * (trajectory)
        % solve for the corrected trajectory and current along correct trajectory        
        function [time_vector_inflated, B, C] = get_matrices_continuous(obj, time_vector)
            [time_vector_inflated, marker] = obj.inflate_time_vector(time_vector); 
            
            dt_vector = diff(time_vector_inflated);

            % construct B matrix
            % the matrix that sums up dt * w1 + dt * w2 ...
            B = kron(tril(repmat(transpose(dt_vector(:)), length(dt_vector), 1)), eye(2));
                        
            % construct C matrix
            % take out the first GPS marker (measurements are drifts)
            marker = marker(2:end); 
            marker_matrix = diag(marker); 
            marker_matrix_row_is_zero = all(marker_matrix == 0, 2);
            marker_matrix(marker_matrix_row_is_zero, :) = []; 
            % replace ones with eye(2)
            marker_matrix = kron(marker_matrix, eye(2));
            
            % these are the points we observe
            C = marker_matrix * B;             
        end
        %% Format into dead-reckoning and drift assuming drifter dynamics
        % the dead-reckoning estimate is stationary, drift computed
        % correspondingly
        function [x_minor, x_dr, dx_dr] = get_measurements(obj, trajectory_en, time_vector, time_vector_inflated)

            % oversample the trajectory
            x_minor = interp1(time_vector, trajectory_en, time_vector_inflated); 
            x_minor = x_minor(1:(end-1), :);
            
            dx_dr = transpose(trajectory_en(2:end, :) - trajectory_en(1, :));
            
            % assume dead reckoning is stationary
            x_dr = repmat(trajectory_en(1, :), size(x_minor, 1), 1);                        
        end
        
        %% super-sample time vector to match obj.dt
        % time_vector_inflated: the super-sampled time vector
        % measurements_marker: an array with 1 if original element, 0 if
        % super-sampled
        function [time_vector_inflated, measurements_marker] = inflate_time_vector(obj, time_vector)
            maximum_dt = obj.dt;
            start_time = time_vector(1); 
            relative_time_vector = time_vector - start_time; 

            time_vector_inflated = [];
            measurements_marker = []; 

            for i = 1:(length(relative_time_vector)-1)
                vector_to_add = relative_time_vector(i):maximum_dt:relative_time_vector(i+1); 
                if vector_to_add(end) == relative_time_vector(i+1)
                    vector_to_add(end) = [];
                end
                marker_to_add = zeros(1, length(vector_to_add)); 
                marker_to_add(1) = 1; 
                time_vector_inflated = [time_vector_inflated, vector_to_add];
                measurements_marker = [measurements_marker, marker_to_add]; 
            end

            %
            time_vector_inflated(end+1) = relative_time_vector(end); 
            time_vector_inflated = time_vector_inflated + start_time; 
            measurements_marker(end+1) = [1];
            
        end
        
    end
    
end
