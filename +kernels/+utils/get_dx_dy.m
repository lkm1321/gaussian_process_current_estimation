% function get_dx_dy(x1, x2)
% helper function for computing [x - x'; y- y'] in a vectorised form to
% speed up derivative computation in stationary kernels

function [dx, dy] = get_dx_dy(x1, x2)
    [grid_x1,grid_x2] = meshgrid(x1(:,1), x2(:, 1));
    [grid_y1,grid_y2] = meshgrid(x1(:,2), x2(:, 2));

    % Grids
    dx = (grid_x2 - grid_x1)';
    dy = (grid_y2 - grid_y1)';
end