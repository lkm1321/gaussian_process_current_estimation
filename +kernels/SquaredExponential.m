classdef SquaredExponential < handle
    properties
        gamma,
        sigma
    end

    methods
        function obj = SquaredExponential(gamma, sigma)
            obj.gamma = gamma;
            obj.sigma = sigma;            
        end

        function K = kernel(obj, x1, x2)
            K = obj.sigma * exp(-0.5 * obj.gamma * pdist2(x1, x2) );
        end

        % derivative kernel w.r.t. first argument
        function dKdx = dx1_kernel(obj, x1, x2)

            [gammaDx, gammaDy] = obj.get_gamma_dx_dy(x1, x2); 
            K_se = obj.kernel(x1, x2); 
                         
            dKdx = (kron(-gammaDy, [1; 0]) + kron(gammaDx, [0; 1])).*kron(K_se, [1; 1]); 
            
        end

        % twice-derivative kernel w.r.t. first argument
        function ddKdxdx = dx1_dx1_kernel(obj, x1, x2)
            [gammaDx, gammaDy] = obj.get_gamma_dx_dy(x1, x2); 
            gammaDxDy = gammaDx.*gammaDy;
 
            K_se = obj.kernel(x1, x2);

            %% Use kronecker product approach instead of loops
            % Can be further optimised by: https://au.mathworks.com/matlabcentral/answers/38288-kron-efficiency#answer_47725
            
            % K = [gamma_y - gamma_y^2 * dy^2, -gamma_x*gamma_y * dx * dy; 
            %      -gamma_x * gamma_y * dx * dy, gamma_x - gamma_x^2 * dx^2 ] * k_se(x1, x2); 
            ddKdxdx = (kron(obj.gamma_y-gammaDy.^2,[1 0;0 0])...
                      +kron(gammaDxDy,[0 1;1 0])...
                      +kron(obj.gamma_x-gammaDx.^2,[0 0;0 1]))...
                      .*kron(K_se,[1 1;1 1]);
 
        end % ddKxdx
    end % public methods

    methods (Access = private)
        function [gammaDx, gammaDy] = get_gamma_dx_dy(obj, x1, x2)
            
            [dx, dy] = kernels.utils.get_dx_dy(x1, x2); 
            gammaDx = obj.gamma*dx;
            gammaDy = obj.gamma*dy;

        end % function get_gamma_dx_dy

    end % private methods
end % classdef