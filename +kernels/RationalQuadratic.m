classdef RationalQuadratic < handle
    properties
        gamma
        sigma
        alpha
    end
    
    methods
        function obj = RationalQuadratic(gamma, sigma, alpha)
            obj.gamma = gamma;
            obj.sigma = sigma;
            obj.alpha = alpha;             
        end
                       
        function K = kernel(obj, x1, x2)
            K = obj.kernel_alpha(x1, x2, obj.alpha);
        end
        
        function dKdx = dx1_kernel(obj, x1, x2)
            [gammaAlphaDx, gammaAlphaDy] = obj.get_gamma_alpha_dx_dy(x1, x2);
            % because of the derivative, need to increment alpha by one            
            K_rq = obj.kernel_alpha(x1, x2, obj.alpha + 1); 
            dKdx = (kron(-gammaAlphaDy, [1; 0]) + kron(gammaAlphaDx, [0; 1])) .* kron(K_rq, [1; 1]);            
        end
        
        function ddKdxdx = dx1_dx1_kernel(obj, x1, x2)
            % just to disable the syntax highlighting
            ddKdxdx = x1 + x2 + obj.alpha;
            error('not implemented');
        end
        
    end
    
    methods (Access = private)
        % allowing alpha to vary so that derivative computations are
        % encapsulated
        function K = kernel_alpha(obj, x1, x2, alpha)
            K = obj.sigma * (1 + (0.5 * obj.gamma / alpha) * pdist2(x1, x2).^2) .^ (-alpha); 
        end
        
        function [gammaAlphaDx, gammaAlphaDy] = get_gamma_alpha_dx_dy(obj, x1, x2)
            [dx, dy] = kernels.utils.get_dx_dy(x1, x2);
            gammaAlphaDx = obj.gamma/obj.alpha * dx;
            gammaAlphaDy = obj.gamma/obj.alpha * dy; 
        end
        
    end
    
    
end