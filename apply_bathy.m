% clear;
clc;
close all; 

restoredefaultpath; 
% addpath(genpath('../../'));
addpath('../ocean-nci/matlab');
addpath('../aus-map/matlab');
addpath('../slocum-fmt-planner');
addpath('../slocum-tools/matlab');

%%

% Load in the bathy image and geotif data
[bathy,R] = readgeoraster("/home/brian/data/Jervis2021/Bathymetry/vincentia_wide2.underwater.utm.tif");
bathy(bathy >= 0) = 0.0;
bathy(bathy < -1E6) = 0.0;

% bathy = imdilate(bathy, offsetstrel('ball', 10, 10));


% Create grid from extents
xlims = R.XWorldLimits;
xstep = R.CellExtentInWorldX;
ylims = R.YWorldLimits;
ystep = R.CellExtentInWorldY;
[x_bathy,y_bathy] = meshgrid(xlims(1):xstep:xlims(2)-1,ylims(1):ystep:ylims(2)-1);
%morphological filtering

%% Get the output. 
% bnd_target = [-35.2  -35.1  150.8  150.9];
bnd_en = [xlims; ylims]; 

p = [1/(35E3)^2 0.2 1.0]; 
% Length scale parameter [0,Inf]
gamma_x = p(1); 
gamma_y = p(1); 
% Standard deviation?
sigma_obs_sq = p(2); 
sigma = p(3) / p(1); 

% GP s
[x_g, y_g] = meshgrid(linspace(bnd_en(1, 1), bnd_en(1, 2), 20), ...
                      linspace(bnd_en(2, 1), bnd_en(2, 2), 20));
X_grid = [x_g(:), y_g(:)];
ogp = OceanGPReduced(X_grid, sigma_obs_sq, gamma_x, gamma_y, sigma);

% get the isocurve
[c, h] = contour(x_bathy, y_bathy, bathy, [-1.0 -1.0]);
% handle matlab's funny output
curves = parse_contour_output(c);

N_sample = 10;
% before adding bathymetry
[U_sample, V_sample] = ogp.sample(x_g(:), y_g(:), N_sample);
figure; hold on;
for i = 1:length(curves)
    plot(curves(i).xy(1, :), curves(i).xy(2, :), 'k');
end
for i = 1:N_sample
    quiver(x_g(:), y_g(:), U_sample(:, i), V_sample(:, i));
end
hold off;

% add bathymetry

for i = 1:length(curves)
    ogp.addBathy(curves(i).xy(1, :)', curves(i).xy(2, :)', -0.5, 1E-2);
end

% after adding bathymetry
[U_sample, V_sample] = ogp.sample(x_g(:), y_g(:), N_sample);
figure; hold on;
for i = 1:length(curves)
    plot(curves(i).xy(1, :)', curves(i).xy(2, :)', 'k');
end

for i = 1:N_sample
    quiver(x_g(:), y_g(:), U_sample(:, i), V_sample(:, i));
end
hold off;



