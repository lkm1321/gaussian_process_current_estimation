# Current estimation from GPS data

This repository contains the code from 'Online estimation of ocean current from sparse GPS data', ICRA'19. 
The paper is available [here](https://arxiv.org/abs/1901.09513). 

## Classes

OceanGP.m: is an implementation of the incomressible GP presented in the paper
OceanGP3D.m: is an extension of incompressible GP to currents varying over depth. 
OceanGPCompressible.m: is an implementation of a "vanila" GP without incompressibility.
OceanGPMM.m: is an implementation of the EM algorithm for current estimation presented in the paper. It makes use of either one of the OceanGP classes listed above. 

## Main scripts

main_ext.m: shows OceanGP class in action. 
main_dr.m: shows OceanGPMM class in action. 
main_dr_sim: shows OceanGPMM class in action with simulated data. Before calling this script, call 'get_simulate_data.m'. 

## Note:

ll usually refers to lat lon
en usually refers to east north (as per UTM coordinate system)
