function [en, zone] = ll2utm(ll)
    lat = ll(:, 1); 
    lon = ll(:, 2); 
    [e, n, zone, ~] = lla2utm([lat(:)'; lon(:)'; zeros(1, length(lat))]); 
    en = [e(:), n(:)]; 
end