function [curves] = parse_contour_output(c)
    curves = [];
    head = 1; 
    while head < size(c, 2)
        curve.level = c(1, head); 
        length = c(2, head); 
        curve.xy = c(:, head + (1:length) );
        head = head + length + 1; 
        curves = [curves; curve];
    end

end