% clear;
clc;
close all; 

restoredefaultpath; 
% addpath(genpath('../../'));
addpath('utils');
addpath('../ocean-nci/matlab');
addpath('../aus-map/matlab');
addpath('../slocum-fmt-planner');
addpath('../slocum-tools/matlab');

%%
% loads variable GPS
load data/Jervis_driftTraj_D2.mat

trajectory_ll = [GPS{1}.Lat, GPS{1}.Lon];
time_vector = GPS{1}.Time; 


%% Get the output. 
% bnd_target = [-35.2  -35.1  150.8  150.9];
bnd_target = [-35.0285  -35.0279  150.778  150.779];
bnd = [-35.0285, 150.778; -35.0279, 150.779];
bnd_en = ll2utm(bnd);

p = [1/(35E3)^2 0.2 1.0]; 
% Length scale parameter [0,Inf]
gamma = p(1); 
% Standard deviation?
sigma_obs_sq = p(2); 
sigma = p(3) / p(1); 

[~, ~, utmzone] = lla2utm([trajectory_ll(1, :), 0]'); 

% GP s

% standard GP
% ogp = OceanGP([], [], [], [], sigma_obs_sq, gamma_x, gamma_y, sigma, 500); 

[x_g, y_g] = meshgrid(linspace(bnd_en(1, 1), bnd_en(2, 1), 20), linspace(bnd_en(1, 2), bnd_en(2, 2), 20));
X_grid = [x_g(:), y_g(:)];
% kernel = kernels.SquaredExponential(gamma, sigma);
kernel = kernels.RationalQuadratic(gamma, sigma, 1E-2);
ogp = OceanGPReduced(kernel, X_grid, sigma_obs_sq);

gamma_gps = 1E-6; 
num_iter = 50; 
dt = 2; 

[lat_jb, lon_jb] = load_JB_boundary();
    
ogpmm = OceanGPMM(ogp, gamma_gps, num_iter, dt, utmzone); 
% Feed in data one at a time into GPMM

[traj_corrected_ll] = ogpmm.update_ll_continuous_multiple({[trajectory_ll, time_vector]});
traj_corrected_ll = traj_corrected_ll{1};
[mLat, mLon] = meshgrid( linspace(-0.002 + bnd(1, 1), 0.002 + bnd(2, 1), 30), ...
                         linspace(-0.002 + bnd(1, 2), 0.002 + bnd(2, 2), 30));

[mU, mV, K] = ogpmm.predict_ll(mLat, mLon); 
[mPhi] = ogpmm.predictPhi_ll(mLat, mLon); 

trK = NaN(size(mLat)); 

Kv = diag(K); 

for i = 1:numel(trK)
    trK(i) = sum(Kv((2*i-1):(2*i)));
end

%% Visualize

clf; hold on; grid on; axis equal;

cc = colormap('hot');
cc = cc(end:-1:1, :);
colormap(cc);
%     [Cphi, hCountourphi] = contour(mLon, mLat, mPhi, 'LineColor', 'k'); 
[C, hContour] = contourf(mLon, mLat, trK, 'LineStyle', 'none');

c = colorbar;
title(c, 'Uncertainty');

% plot(lon_jb, lat_jb);
plot(trajectory_ll(:, 2), trajectory_ll(:, 1), 'kx');
plot(traj_corrected_ll(:, 2), traj_corrected_ll(:, 1), 'g', 'LineWidth', 4);
hq = quiver(mLon, mLat, mU, mV);
if (exist('u_func', 'var') && exist('v_func', 'var'))
    mEN = ll2utm([mLat(:), mLon(:)]);
    mU_true = u_func(mEN(:, 1), mEN(:, 2));
    mV_true = v_func(mEN(:, 1), mEN(:, 2));
    hq_true = quiver(mLon(:), mLat(:), mU_true, mV_true);    
end

drawnow;


%% Get the output. 

% bnd_target = [-35.3  -35.1  150.7  151];
% 
% if (~exist('ocp', 'var'))
%     ocp = OceanCurrents(bnd_target, 'sinterp', 0.01, 'avgdepth', [5, 180]);
%     save ocp_ex.mat ocp
% end

% [c_struct, K] = ogpmm.get_struct(ocp.data(1));  
% 
% %% Visualize 
% 
% mLat = c_struct.mLat(:, :, 1); 
% mLon = c_struct.mLon(:, :, 1); 
% mU = c_struct.mU(:, :, 1);
% mV = c_struct.mV(:, :, 1); 

%% Visualize

                   
figure; hold on; grid on; axis equal;
plot(lon_jb, lat_jb, 'black-');

cc = colormap('hot'); 
cc = cc(end:-1:1, :); 
colormap(cc); 

contour(mLon, mLat, mPhi); 
% [C, hContour] = contourf(mLon, mLat, trK, linspace(min(trK), max(trK), 10), 'LineStyle', 'none');

c = colorbar;
title(c, 'Uncertainty'); 

plot(trajectory_ll(:, 2), trajectory_ll(:, 1), 'gx');
hq = quiver(mLon, mLat, mU, mV); 
if (exist('u_func', 'var') && exist('v_func', 'var') && exist('phi_func', 'var') )
    mEN = ll2utm([mLat(:), mLon(:)]); 
    mU_true = u_func(mEN(:, 1), mEN(:, 2));
    mV_true = v_func(mEN(:, 1), mEN(:, 2)); 
    mPhi_true = phi_func(mEN(:, 1), mEN(:, 2)); 
    hq_true = quiver(mLon(:), mLat(:), mU_true, mV_true); 
    hphi_true = contour(mLon, mLat, reshape(mPhi_true, size(mLon)) ); 
end

return; 