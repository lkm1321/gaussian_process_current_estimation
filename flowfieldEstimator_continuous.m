function [ogpmm, bnd, traj_corrected_ll, drifterTraj] = flowfieldEstimator_continuous(xlsFile, numFloat, bnd, p, gamma_gps, num_iter, dt)

%% Tuning parameters
% Length scale parameter [0,Inf]
gamma = p(1); 
% Standard deviation?
sigma_obs_sq = p(2); 
sigma = p(3) / p(1); 

%% Extract trajectory
latArray = [];
lonArray = [];
for nF = 1:numFloat
    sheetNum = sprintf('Sheet%d',nF);
    drifterTraj{nF} = readmatrix(xlsFile, 'Sheet', sheetNum);
    latArray = [latArray; drifterTraj{nF}(:,1)];
    lonArray = [lonArray; drifterTraj{nF}(:,2)];
end

%% Boundary

latMin = min(latArray);
latMax = max(latArray);
lonMin = min(lonArray);
lonMax = max(lonArray);

bnd = [latMin - 0.2 * (latMax - latMin), lonMin - 0.2 * (lonMax - lonMin); 
       latMax + 0.2 * (latMax - latMin), lonMax + 0.2 * (lonMax - lonMin)];
bnd_en = ll2utm(bnd);

[x_g, y_g] = meshgrid(linspace(bnd_en(1, 1), bnd_en(2, 1), 20), linspace(bnd_en(1, 2), bnd_en(2, 2), 20));
X_grid = [x_g(:), y_g(:)];
    

%% utmzone
% Get trajectory for UTM zone
traj_temp = [drifterTraj{1}(:,1), drifterTraj{1}(:,2)];
[~, ~, utmzone] = lla2utm([traj_temp(1, :), 0]'); 

%% Flow field estimator
kernel = kernels.SquaredExponential(gamma, sigma); 
ogp = OceanGPReduced(kernel, X_grid, sigma_obs_sq);
ogpmm = OceanGPMM(ogp, gamma_gps, num_iter, dt, utmzone); 
% process the trajectories
traj_corrected_ll = ogpmm.update_ll_continuous_multiple(drifterTraj); 

end