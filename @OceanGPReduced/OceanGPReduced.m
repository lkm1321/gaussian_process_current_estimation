classdef OceanGPReduced < matlab.mixin.Copyable
    properties
        X_grid        % grid_size x 2 vector
        beta_mean     % grid_size x 1 vector
        beta_cov      % grid_size x grid_size matrix
        sigma_obs_sq    % measurement noise
        process_noise = 0.% process noise
        kernel % the kernel
    end
    
    properties (Dependent)
        num_grid_points
    end
    
    methods
       % X_grid: Nx2 grid for beta       
       % sigma_obs_sq: scalar; variance of measurement
       % gamma_x: scalar; length scale parameter in x. 
       % gamma_y: scalar; length scale parameter in y. 
       % sigma: scalar; variance scale of the kernel. 
       % reserve_size: integer; size preallocated to variables. 
       function obj = OceanGPReduced(kernel, X_grid, sigma_obs_sq)

           obj.kernel = kernel; % a kernel.* object
           obj.X_grid = X_grid;
           obj.beta_mean = zeros(obj.num_grid_points, 1);
           obj.beta_cov = eye(obj.num_grid_points);
           
           % GP properties           
           obj.sigma_obs_sq = sigma_obs_sq;  
       end
       
       function n = get.num_grid_points(obj)
           n = size(obj.X_grid, 1); 
       end
        
        function addSample(obj, X_sample, Y_sample, U_sample, V_sample)
            [Y_pred, K_pred, K_pred_beta] = obj.predict_internal([X_sample(:), Y_sample(:)]);
            Y_measured = [U_sample(:), V_sample(:)]';
            Y_measured = Y_measured(:); 
            Y_measured_cov = K_pred + obj.sigma_obs_sq * eye(size(K_pred, 1));
            obj.beta_cov =  obj.beta_cov + 1E-3 * eye(size(obj.beta_cov, 1));
            obj.beta_mean = obj.beta_mean + K_pred_beta' ...
                          * ( (Y_measured_cov) \ (Y_measured - Y_pred) );
                      
            obj.beta_cov = obj.beta_cov - K_pred_beta' ...
                          * ( (Y_measured_cov) \ K_pred_beta ); 
                      
        end
        
%         function addBathy(obj, X_bathy, Y_bathy, z0, sigma)
%             X_b = [X_bathy(:), Y_bathy(:)];
%             
%             meas_mat = obj.k_se(X_b, obj.X_grid);
% 
%             % this C matrix returns difference of stream values along the
%             % bathy contour
%             C_matrix_diagonal = diag(ones(size(X_b, 1), 1)); 
%             C_matrix_off_diagonal = diag(-ones(size(X_b, 1) - 1, 1), 1);
%             C_matrix = C_matrix_diagonal + C_matrix_off_diagonal; 
%             C_matrix = C_matrix(1:(end-1), :);
%             
%             overall_meas_mat = C_matrix * meas_mat;
%             
%             Y_pred = overall_meas_mat * obj.beta_mean;
%             K_pred_beta = overall_meas_mat * obj.beta_cov; 
%             K_pred = K_pred_beta * overall_meas_mat';
%             
%             Y_measured = zeros(size(C_matrix, 1), 1); 
%             Y_measured_cov = K_pred + sigma * eye(size(K_pred, 1)); 
%             
%             obj.beta_mean = obj.beta_mean + K_pred_beta' * ( (Y_measured_cov) \ (Y_measured - Y_pred)); 
% 
%             difference = K_pred_beta' * mldivide(Y_measured_cov, K_pred_beta);
%             difference = 0.5 * (difference + difference'); 
%             obj.beta_cov = obj.beta_cov - difference; 
%         end
        
        function [U_sample, V_sample] = sample(obj, X_query, Y_query, N_sample)
            beta_L = chol(obj.beta_cov); 
            beta_sample = obj.beta_mean + beta_L * randn(size(obj.beta_mean, 1), N_sample);
            meas_mat = obj.getKMat([X_query(:), Y_query(:)]);
            Y_sample = meas_mat * beta_sample; 
            U_sample = Y_sample(1:2:(end-1), :); 
            V_sample = Y_sample(2:2:end, :);             
        end
                
        function [U_query, V_query, K_query] = predict(obj, X_query, Y_query)
            [Y_pred, K_query] = obj.predict_internal([X_query(:), Y_query(:)]); 
            U_query = Y_pred(1:2:(end-1));
            V_query = Y_pred(2:2:end);             
        end

       function [phi_query, K_query] = predictPhi(obj, X_query, Y_query)

          X_q = [X_query(:), Y_query(:)]; 
          
          meas_mat = obj.kernel.kernel(X_q, obj.X_grid); 
          
          phi_query = meas_mat * obj.beta_mean;
          K_query = meas_mat * obj.beta_cov * meas_mat'; 
          
       end   
    end 
    
    methods (Access = private)
        function [Y, K_Y, K_Y_beta] = predict_internal(obj, X_q)
            meas_mat = obj.getKMat(X_q); 
            Y = meas_mat * obj.beta_mean;
            K_Y = meas_mat * obj.beta_cov * meas_mat' + obj.sigma_obs_sq*eye(size(meas_mat, 1));            
            K_Y_beta = meas_mat * obj.beta_cov; 
        end
        function K = getKMat(obj, X_q)
            K = obj.kernel.dx1_kernel(X_q, obj.X_grid); 
        end        
    end
end
    
    