N_grid = 20;

[x_g, y_g] = meshgrid(linspace(-2, 12, N_grid));

X_g = [x_g(:), y_g(:)];

ogpr = OceanGPReduced(X_g, 1E-6, 1.0, 1.0, 1.0);

u = sin(X_g(:, 1)/pi) .* cos(X_g(:, 2)/pi);
v = -cos(X_g(:, 1)/pi) .* sin(X_g(:, 2)/pi);

ogpr.addSample(X_g(:, 1), X_g(:, 2), u, v);
[u_p, v_p, K] = ogpr.predict(X_g(:, 1), X_g(:, 2)); 
[phi_query] = ogpr.predictPhi(X_g(:, 1), X_g(:, 2)); 

figure;
hold on;
quiver(X_g(:, 1), X_g(:, 2), u, v, 'r');
quiver(X_g(:, 1), X_g(:, 2), u_p, v_p, 'g');
contour(reshape(X_g(:, 1), N_grid, N_grid), ...
        reshape(X_g(:, 2), N_grid, N_grid), ...
        reshape(phi_query, N_grid, N_grid), 'k'); 
hold off;